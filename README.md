# Module Template

This project is a template for Hackanizer modules' projects.

Follow these steps to properly configure your repository:

1. change `module/.env` file with your project's info

2. install prisma via NPM
  `npm install -g prisma@1.7.0`

3. after you change your `api/prisma/datamodel.graphql` you have to run `prisma deploy` inside the folder `api/prisma`
